package com.zuitt;
import javax.sound.midi.Soundbank;
import java.sql.SQLOutput;
import java.util.Scanner;

public class Main {
    public static void main (String [] args ) {

        Scanner scannerName = new Scanner (System.in);

        System.out.println("First Name:");
        String firstName = scannerName.nextLine();

        System.out.println("Last Name:");
        String lastName = scannerName.nextLine();

        System.out.println("First Subject Grade:");
        double firstSubject = scannerName.nextDouble();

        System.out.println("Second Subject Grade");
        double secondSubject = scannerName.nextDouble();

        System.out.println("Third Subject Grade");
        double thirdSubject = scannerName.nextDouble();
        double averageGrade= (firstSubject + secondSubject + thirdSubject)/3;

        System.out.println("Good day " + firstName + " " + lastName + ". Your average grade is: " + averageGrade);

    }
}
